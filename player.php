<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">


    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" href="image/Logo_TYPOS.png" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cinzel|PT+Serif" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cinzel+Decorative" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Fredoka+One|Pathway+Gothic+One" rel="stylesheet"> 
    <title>Typos Player</title>
    <link rel="stylesheet" href="css/player.css">
    <link rel="stylesheet" href="css/background.css">
    <?php include("includes/animation_load.php") ?>
</head>

<body>
    <!--<img src="image\Logo_TYPOS.png" class="element-animation" id="testImg">-->
    <?php include("includes/connexion.php") ?>

    <?php 
    $id_music = 1;
   

    $music = $bdd->prepare('SELECT * FROM music WHERE id = ?');
    $music->execute(array($id_music));
    $reqmusic = $music->fetch();


    { ?>

    
          <div id="panel" class="row">
            <div id="cover_sticky" class="col s5" data-sticky-container >
              <div id="cover" class="sticky" data-sticky data-stick-to="top-right" data-margin-top="2">
                <img src="<?php echo $reqmusic['url_cover'] ?>" width="350vw">
              </div>
            </div>

            <div id="donneAuteur" class="col s6" >
              <h2 id="now"> NOW PLAYING </h2>
              <hr>
              <br>
              <p> Name : <?php echo $reqmusic['name'] ?> <br></p>
              <p> Artiste : <?php echo $reqmusic['artiste'] ?> <br></p>
              <p> Album : <?php echo $reqmusic['album'] ?> <br></p>
              <p> Genre : <?php echo $reqmusic['genre'] ?> <br></p>

            </div>

            <div id="lecteur" class="col s12">
              <div id="previous"><i class=" material-icons">skip_previous</i></div>
              <div id="stop"><i class=" material-icons">stop</i></div>
              <div id="next" href="partials/post.php"><i class=" material-icons">skip_next</i></div>
              <div id="previous"><i class=" material-icons">loop</i></div>
              <audio controls style="width:85vw; background:#f2282e;">
                <source src="<?php echo $reqmusic['url_music'] ?>" type="audio/mpeg">
                Your browser does not support the audio element.
              </audio>
              <img src="image\Logo_TYPOS.png" width="20px" class="element-animation" id="testImg">
              
              
            </div>
             
            
          
   <?php 
} ?>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
  
  <div class="playlist">
            <table id="table">
                      <thead>
                        <tr>
                            <th>N°</th>
                            <th>Titre</th>
                            <th>Artiste</th>
                            <th>Album</th>
                            <th>Genre</th>
                            <th>Duree</th>
                        </tr>
                      </thead> 
                      <tbody>
    <?php 

            $playliste = $bdd->prepare('SELECT * FROM music ORDER BY id ASC LIMIT 0, 10');
            $playliste->execute(array());

                while ($reqplayliste = $playliste->fetch())
                { ?>
                            
                              <tr>
                                <td><?php echo $reqplayliste['id'] ?></td>
                                <td><?php echo $reqplayliste['name'] ?></td>
                                <td><?php echo $reqplayliste['artiste'] ?></td>
                                <td><?php echo $reqplayliste['album'] ?></td>
                                <td><?php echo $reqplayliste['genre'] ?></td>
                                <td><?php echo $reqplayliste['duree'] ?></td>
                              </tr>
                            
        <?php  } ?>
               </tbody>
              </table>

              <?php  $playliste->closeCursor(); ?>




  </div>
   
</div>


                
            
            


   <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>  
     <!--JavaScript at end of body for optimized loading-->
     <script type="text/javascript" src="js/jquery.js"></script>
   <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/JsMain.js"></script>
    <script type="text/javascript" src="js/player.js"></script>


    <br>
</body>

</html>
<?php echo "<style>

.element-animation{
animation: animationFrames linear 3s;
animation-iteration-count: infinite;
transform-origin: 50% 50%;
-webkit-animation: animationFrames linear 3s;
-webkit-animation-iteration-count: infinite;
-webkit-transform-origin: 50% 50%;
-moz-animation: animationFrames linear 3s;
-moz-animation-iteration-count: infinite;
-moz-transform-origin: 50% 50%;
-o-animation: animationFrames linear 3s;
-o-animation-iteration-count: infinite;
-o-transform-origin: 50% 50%;
-ms-animation: animationFrames linear 3s;
-ms-animation-iteration-count: infinite;
-ms-transform-origin: 50% 50%;
}

@keyframes animationFrames{
0% {
transform:  rotate(0deg) ;
}
100% {
transform:  rotate(360deg) ;
}
}

@-moz-keyframes animationFrames{
0% {
-moz-transform:  rotate(0deg) ;
}
100% {
-moz-transform:  rotate(360deg) ;
}
}

@-webkit-keyframes animationFrames {
0% {
-webkit-transform:  rotate(0deg) ;
}
100% {
-webkit-transform:  rotate(360deg) ;
}
}

@-o-keyframes animationFrames {
0% {
-o-transform:  rotate(0deg) ;
}
100% {
-o-transform:  rotate(360deg) ;
}
}

@-ms-keyframes animationFrames {
0% {
-ms-transform:  rotate(0deg) ;
}
100% {
-ms-transform:  rotate(360deg) ;
}
}

#testImg {
    animation-timing-function: linear;
    animation-name: animationFrames;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    transition: all 0.4s;
}

#testIdmg:hover {
    filter: blur(10px) contrast(80%);
    transform: rotate(-0.5turn);
    transition: all 1s;
}
</style>";
?>
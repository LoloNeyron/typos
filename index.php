<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="icon" type="image/png" href="image/Logo_TYPOS.png" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cinzel|PT+Serif" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cinzel+Decorative" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Fredoka+One|Pathway+Gothic+One" rel="stylesheet"> 
</head>

<body>

<video autoplay muted loop id="myVideo">
  <source src="video/Background.mp4" type="video/mp4">
</video>

    <div>
        <div class="row">
            <div class="col s12 m3 center-align">
                <img src="image/Logo_TYPOS.png" alt="logo du lecteur de musique Typos." id="logo">
            </div>
            <div class="col s12 m3 center-align">
                <h1 id="title_Logo">TYPOS</h1>
                <br>
            </div>
            <div class="col s3 m0"></div>
            <div class="col s9 m6">
            
                <a class="waves-effect waves-light btn btn-large modal-trigger bouttonConnexions" id="conexions" href="#modalConnexion">
                    <i class="material-icons right">music_note</i>
                    Connexions
                </a>
            </div>
        </div>
    </div>

    <div id="modalConnexion" class="modal bottom-sheet">
        <div class="modal-content container">
            <div class="row">
                <div class="col s8"><h4>Connexion</h4></div>
            </div>
            <div class="row">
                <div clas="col s4"><p>Connectez vous avec un pseudo:</p></div>
            </div>
        </div>
        <form action="player.php" method="post">
            <div class="container">
                <div class="row">
                    <div class="col s3"></div>
                    <div class="col s6"><input name="pseudo" required type="text" placeholder="Super-man"></div>
                </div>
                <div class="row">
                    <div class="col s3"></div>
                    <div class="col s6"><input class="waves-effect waves-light btn btn-large modal-trigger bouttonConnexions" value="Ecoutez" style="color: white;"type="submit"></div>
                </div>
            </div>
        </form>
        </div>
    </div>





    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery.js"></script>
   <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/JsMain.js"></script>
</body>

</html>